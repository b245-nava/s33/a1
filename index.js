// console.log("yeet");

// Fetch request (all items)

fetch("https://jsonplaceholder.typicode.com/todos",
	{ method: "GET" })
.then(resp => resp.json())
.then(result => console.log(result));

// Title Array w/ .map()

fetch("https://jsonplaceholder.typicode.com/todos",
	{ method: "GET" })
.then(resp => resp.json())
.then(data => {
	let title = data.map(elem => elem.title)
	console.log(title);
});

// Fetch request w/ GET (single to do list item)
fetch("https://jsonplaceholder.typicode.com/todos/13", {
	method: "GET"
})
.then(resp => resp.json())
.then(result => console.log(result));

// Title + Status of retrieved item
fetch("https://jsonplaceholder.typicode.com/todos/13", {
	method: "GET"
})
.then(resp => resp.json())
.then(data => console.log(`The item "${data.title}" on the list has the status ${data.completed}.`));

// POST to create item
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
			},
		body: JSON.stringify({
			title: "New Item",
			userId: 1,
			completed: false
		})
	})
.then(response => response.json())
.then(result => console.log(result));

// PUT to update a to do item
fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated Item",
		userId: "still 1",
		completed: true
	})
})
.then(response => response.json())
.then(result => console.log(result));

// Update w/ changed data structure
fetch("https://jsonplaceholder.typicode.com/todos/42", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Release 'Ring Ding Dong'",
		description: "Banned during KSATS",
		status: "completed",
		dateCompleted: "Oct. 14, 2009",
		userId: "SHINee"
	})
})
.then(resp => resp.json())
.then(result => console.log(result));

// PATCH to update to do list item
fetch("https://jsonplaceholder.typicode.com/todos/101", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "masakit na po daliri ko sir"
	})
})
.then(resp => resp.json())
.then(result => console.log(result));

// Update item's status and adding date of completion
fetch("https://jsonplaceholder.typicode.com/todos/200", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		statusUpdate: "Dec. 31, 9999"
	})
})
.then(resp => resp.json())
.then(result => console.log(result));

// Delete item
fetch("https://jsonplaceholder.typicode.com/todos/55", {
	method: "DELETE"
})
.then(res => res.json())
.then(result => console.log(result));